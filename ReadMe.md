## CLI tool for word play

# Pre-requisites
- node.js > 7

# Instructions

- npm install
- For debug mode
  DEBUG=test ./dict [command1] [command2]
- For normal mode
  ./dict [command1] [command2]
- For help
  ./dict -h or ./dict --help



