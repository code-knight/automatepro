var promise = require('bluebird');
var helpers = require('./helpers');
var getDefinitionsPromisified = promise.promisify(helpers.getDefinitions);
var getExamplesPromisified = promise.promisify(helpers.getExamples);
var getAntonymsPromisified = promise.promisify(helpers.getAntonyms);
var getSynonymsPromisified = promise.promisify(helpers.getSynonyms);
var getWordOfTheDayPromisified = promise.promisify(helpers.getWordOfTheDay);
var getRandomWordPromisified = promise.promisify(helpers.getRandomWord);
var utils = require('./utils');
var debug = require('debug')('test');

function getDefinitionForWord(word) {
	getDefinitionsPromisified(word).then((wordDefinitionsArray) => {
		utils.displayDefinitons(wordDefinitionsArray);
	}).catch((err) => {
		console.log(err);
	})
}

function getExamplesForWord(word) {
	getExamplesPromisified(word).then((examplesArray) => {
		utils.displayExamples(examplesArray);
	}).catch((err) => {
		console.log(err);
	})
}

function getAntonymsForWord(word) {
	getAntonymsPromisified(word).then((wordsArray) => {
		utils.displayAntonyms(wordsArray);
	}).catch((err) => {
		console.log(err);
	})
}

function getSynonymsForWord(word) {
	getSynonymsPromisified(word).then((wordsArray) => {
		utils.displaySynonyms(wordsArray);
	}).catch((err) => {
		console.log(err);
	})
}

function getAllDetailsForWordOfTheDay() {
	debug('in all details for the word');
	getWordOfTheDayPromisified().then((word) => {
		debug(word);
		utils.displayWordOfTheDay(word);
		return getAllDetailsForWord(word);
	}).catch((err) => {
		console.log(err);
	})
}

function getAllDetailsForWord(word) {
	getDefinitionsPromisified(word).then((wordDefinitions) => {
		utils.displayDefinitons(wordDefinitions);
		return getSynonymsPromisified(word);
	}).then((synonyms) => {
		utils.displaySynonyms(synonyms);
		return getAntonymsPromisified(word);
	}).then((antonyms) => {
		utils.displayAntonyms(antonyms);
		return getExamplesPromisified(word);
	}).then((examples) => {
		utils.displayExamples(examples);
	}).catch((err) => {
		console.log(err);
	})
}

function getRandomWordDefinition(cb) {
	var quitWord = '';
	getRandomWordPromisified().then((word) => {
		quitWord = word;
		return getDefinitionsPromisified(word)
	}).then((wordDefinitions) => {
		if (wordDefinitions.length) {
			utils.displayDefinitionForWord(wordDefinitions, quitWord, cb);
		} else {
			playWithWord(cb);
		}
	}).catch((err) => {
		console.log(err);
	})
}

function getRandomWordAntonym(cb) {
	var quitWord = '';
	getRandomWordPromisified().then((word) => {
		quitWord = word;
		return getAntonymsPromisified(word)
	}).then((antonyms) => {
		if (antonyms.length) {
			utils.displayAntonymForWord(antonyms, quitWord, cb);
		} else {
			playWithWord(cb);
		}
	}).catch((err) => {
		console.log(err);
	})
}

function getRandomWordSynonym(cb) {
	var quitWord = '';
	getRandomWordPromisified().then((word) => {
		quitWord = word;
		return getSynonymsPromisified(word)
	}).then((synonyms) => {
		if (synonyms.length) {
			utils.displaySynonymForWord(synonyms, quitWord, cb);
		} else {
			playWithWord(cb);
		}
	}).catch((err) => {
		console.log(err);
	})
}

function playWithWord(cb) {
	var funcArray = [getRandomWordDefinition, getRandomWordAntonym, getRandomWordSynonym];
	var index = utils.getRandomIndexForArray(funcArray);
	funcArray[index](cb);
}

function getHintForTheWord(word, cb) {
	var funcArray = [getDefinitionForTheWord, getSynonymForTheWord, getAntonymForTheWord];
	var index = utils.getRandomIndexForArray(funcArray);
	funcArray[index](word, cb);
}

function getDefinitionForTheWord(word, cb) {
	getDefinitionsPromisified(word).then((wordsArray) => {
		if (wordsArray) {
			var index = utils.getRandomIndexForArray(wordsArray);
			utils.displayDefinitionForWord(wordsArray, word,cb);
		} else {
			getHintForTheWord(word, cb);
		}
	})
}

function getSynonymForTheWord(word, cb) {
	debug(word,'syn to be found for this word');
	getSynonymsPromisified(word).then((synonymsArray) => {
		if (synonymsArray) {
			utils.displaySynonymForWord(synonymsArray, word,cb)
		} else {
			getHintForTheWord(word, cb);
		}
	})
}

function getAntonymForTheWord(word, cb) {
	getAntonymsPromisified(word).then((antonymsArray) => {
		if (antonymsArray) {
			var index = utils.getRandomIndexForArray(antonymsArray);
			utils.displayAntonymForWord(antonymsArray, word,cb)
		} else {
			getHintForTheWord(word, cb);
		}
	})
}

function checkWord(word, actualWord,cb) {
	debug(actualWord, 'actualword');
	if (word === actualWord)
		cb(true);
	else {
		getSynonymsPromisified(actualWord).then((wordsArray) => {
			debug(wordsArray,'wordsArray');
			debug(actualWord,'actualWord');
			var f = wordsArray.filter((x) => x.relationshipType == 'synonym');
			if (f.length && f[0].words.includes(word)) {
				cb(true);
			} else {
				cb(false);
			}
		}).catch((err) => {
			debug(err);
		})
	}
}

module.exports = {
	getDefinitionForWord,
	getExamplesForWord,
	getAntonymsForWord,
	getSynonymsForWord,
	getAllDetailsForWordOfTheDay,
	getAllDetailsForWord,
	getHintForTheWord,
	playWithWord,
	checkWord
}