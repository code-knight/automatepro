#!/usr/bin/env node

var program = require('commander');
var wordplay = require('./common');
var game = require('./game');

program
  .version('0.0.1')
  .description('An cli tool for wordnik api')
  .option('def, --definition <word>', 'Get definition for the word')
  .option('syn, --synonym <word>', 'Get synonym for the word')
  .option('ant, --antonym <word>', 'Get antonym for the word')
  .option('ex,--example <word>', 'Get examples for the word')
  .option('dict, --dictionary <word>', 'Get synoyms,antonyms, definitons and exampples of the word')
  .option('play, --play', 'PLay the word game!!')
  .parse(process.argv);

if (program.definition) wordplay.getDefinitionForWord(program.definition);
if (program.synonym) wordplay.getSynonymsForWord(program.synonym);
if (program.antonym) wordplay.getAntonymsForWord(program.antonym);
if (program.example) wordplay.getExamplesForWord(program.example);
if (program.dictionary) wordplay.getAllDetailsForWord(program.dictionary);
if (program.play) game.beginGame(wordplay);

if(process.argv[2] == undefined){
  wordplay.getAllDetailsForWordOfTheDay()
}









