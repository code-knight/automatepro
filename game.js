const inquirer = require('inquirer');
var wordie = require('./common');
var debug= require('debug')('test');

function takeInput(word) {
	inquirer
		.prompt({
			name: 'answers',
			type: 'input',
			message: 'Guess the word?',
			validate: function (value) {
				if (value.length) {
					return true;
				} else {
					return 'Ahem,Cant continue without guess !';
				}
			}
		})
		.then(answers => {
			takeWord(answers, word);
		})
}

function beginGame() {
	debug('in begin game');
	wordie.playWithWord((word) => {
		debug(word, 'actual word');
		if (!word) {
			beginGame();
		} else {
			console.log('Game Begins!!');
			takeInput(word);
		}
	})
}
function takeWord(word, actualWord) {
	debug('in take word');
	debug(word.answers, actualWord,'act and word');
	wordie.checkWord(word.answers, actualWord, (isCorrectWord) => {
		debug(isCorrectWord,'correct word check');
		if (isCorrectWord) {
			console.log('Bingo! Correct answer');
		}
		else {
			console.log('incorrect');
			options(actualWord);
		}
	});
}

function options(word) {
	var directionsPrompt = {
		type: 'list',
		name: 'direction',
		message: 'What next?',
		choices: ['try again', 'Hint', 'quit', 'play again']
	};

	inquirer.prompt(directionsPrompt).then(choice => {
		if (choice.direction === 'try again') {
			takeInput(word);
		} else if (choice.direction === 'Hint') {
			debug(' actual word is', word, 'we have to get hint fot his');
			wordie.getHintForTheWord(word, (hintWord) => {
				debug('got word from get Hint', hintWord)
				if (!hintWord) console.log('Bad luck, no hint now :D');
				takeInput(word);
			})
		} else if (choice.direction === 'quit') {
			console.log('The word is :', word);
			console.log("======================================");
			wordie.getAllDetailsForWord(word);
		} else {
			beginGame(word);
		}
	});
}



module.exports = {
	beginGame
}