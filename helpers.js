var request = require('request');
var urls = require('./urlSetup');
var debug = require('debug')('test');

function getDefinitions(word, cb) {
	debug(word,'indef word');
	var getDefinitionUrl = urls.getDefinitionsUrl(word);
	request(getDefinitionUrl, (err, res, body) => {
		if (!err) {
			var wordsArray = JSON.parse(body);
			cb(null, wordsArray);
		} else {
			cb(err, null);
		}

	})
}

function getSynonyms(word, cb) {
	var synonymsUrl = urls.getRelatedWordsUrl(word);
	debug(synonymsUrl,'syn url');
	request(synonymsUrl, (err, res, body) => {
		if (!err) {
			var wordsArray = JSON.parse(body);
			cb(null, wordsArray);
		} else {
			cb(err, null);
		}
	})
}

function getAntonyms(word, cb) {
	var antonymsUrl = urls.getRelatedWordsUrl(word);
	debug(antonymsUrl,'ant url');
	request(antonymsUrl, (err, res, body) => {
		if (!err) {
			var wordsArray = JSON.parse(body);
			cb(null, wordsArray);
		} else {
			cb(err, null);
		}
	})
}

function getExamples(word, cb) {
	var examplesUrl = urls.getExamplesUrl(word);
	request(examplesUrl, (err, res, body) => {
		debug(body);
		if (!err) {
			var body = JSON.parse(body);
			cb(null, body.examples);
		} else {
			cb(err, null);
		}

	})
}

function getWordOfTheDay(cb) {
	var wordOfTheDayUrl = urls.getWordOfTheDayUrl();
	debug(wordOfTheDayUrl.toString());
	request(wordOfTheDayUrl, (err, res, body) => {
		if (!err) {
			var body = JSON.parse(body);
			var wordOfTheDay = body.word;
			cb(null, wordOfTheDay);
		} else {
			cb(err, null);
		}

	})
}

function getRandomWord(cb) {
	var randomWordUrl = urls.getRandomWordUrl();
	request(randomWordUrl, (err, res, body) => {
		if (!err) {
			var body = JSON.parse(body);
			//Has to be removed later
			cb(null, body.word);
		} else {
			cb(err, null);
		}

	})
}


module.exports = {
	getDefinitions,
	getExamples,
	getAntonyms,
	getSynonyms,
	getWordOfTheDay,
	getRandomWord,
}