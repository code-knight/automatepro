var urlParts = require('./config/test.json').urls;
var urljoin = require('url-join');
var def = urlParts.definitionsUrl;
var ran = urlParts.randomWordUrl;
var ex = urlParts.examplesUrl;
var re =  urlParts.relatedWordssUrl;
var request = require('request');
var debug = require('debug')('test');

function getDefinitionsUrl(word){
  debug('in def url');
  debug(urljoin(urlParts.wordnik_url,word,def.definitions,`?limit=200&includeRelated=true&useCanonical=${def.useCanonical}&includeTags=${def.includeTags}&api_key=${urlParts.api_key}`));
  return urljoin(urlParts.wordnik_url,word,def.definitions,`?limit=200&includeRelated=true&useCanonical=${def.useCanonical}&includeTags=${def.includeTags}&api_key=${urlParts.api_key}`);
}

function getExamplesUrl(word){
  debug('in ex url');
  debug(urljoin(urlParts.wordnik_url,word,ex.examples,`?includeDuplicates=${ex.includeDuplicates}&useCanonical=${ex.useCanonical}&skip=${ex.skip}&limit=${ex.limit}&api_key=${urlParts.api_key}`));
  return urljoin(urlParts.wordnik_url,word,ex.examples,`?includeDuplicates=${ex.includeDuplicates}&useCanonical=${ex.useCanonical}&skip=${ex.skip}&limit=${ex.limit}&api_key=${urlParts.api_key}`)
}

function getRandomWordUrl(){
  debug('in ran url');
  debug(urljoin(ran.url,`?hasDictionaryDef=${ran.hasDictionaryDef}&minCorpusCount=${ran.minCorpusCount}&maxCorpusCount=${ran.maxCorpusCount}&minDictionaryCount=${ran.minDictionaryCount}&maxDictionaryCount=${ran.maxDictionaryCount}&minLength=${ran.minLength}&maxLength=${ran.maxLength}&api_key=${urlParts.api_key}`));
  return urljoin(ran.url,`?hasDictionaryDef=${ran.hasDictionaryDef}&minCorpusCount=${ran.minCorpusCount}&maxCorpusCount=${ran.maxCorpusCount}&minDictionaryCount=${ran.minDictionaryCount}&maxDictionaryCount=${ran.maxDictionaryCount}&minLength=${ran.minLength}&maxLength=${ran.maxLength}&api_key=${urlParts.api_key}`);
}

function getRelatedWordsUrl(word){
  debug('in rel url');
  debug(urljoin(urlParts.wordnik_url,word,re.relatedWords,`?useCanonical=${re.useCanonical}&limitPerRelationshipType=${re.limitPerRelationshipType}&api_key=${urlParts.api_key}`));
  return urljoin(urlParts.wordnik_url,word,re.relatedWords,`?useCanonical=${re.useCanonical}&limitPerRelationshipType=${re.limitPerRelationshipType}&api_key=${urlParts.api_key}`)
}

function getWordOfTheDayUrl(){
  debug('in word url');
  return urljoin(urlParts.wordOfTheDayUrl.url,`?api_key=${urlParts.api_key}`);
}

module.exports={
  getDefinitionsUrl,
  getWordOfTheDayUrl,
  getRandomWordUrl,
  getRelatedWordsUrl,
  getExamplesUrl
}
