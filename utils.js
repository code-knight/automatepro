var debug = require('debug')('test');

function displayDefinitons(wordDefinitionsArray) {
	console.log('Definitions:')
	debug(wordDefinitionsArray);
	if (wordDefinitionsArray.length) {
		for (var i in wordDefinitionsArray) {
			console.log(i + 1 + '. ' + wordDefinitionsArray[i].text);
		}
	} else {
		console.log('Sorry,Definitions for the word not available');
	}
	printSeparator();
}

function displayExamples(examplesArray) {
	console.log('Examples:');
	debug(examplesArray);
	if (examplesArray.length) {
		for (var a in examplesArray) {
			console.log(+a + 1 + '. ' + examplesArray[a].text);
		}
	} else {
		console.log('Sorry, Examples for the word not available');
	}
	printSeparator();
}


function displayAntonyms(wordsArray) {
	console.log('Antonyms:');
	if (wordsArray.length) {
		var filterArray = wordsArray.filter((x) => x.relationshipType == 'antonym');
		if (filterArray.length) {
			for (var i in filterArray[0].words) {
				console.log(+i+1+'. '+filterArray[0].words[i])
			}
		} else {
			console.log('Sorry,Antonyms for the word not available');
		}
	} else {
		console.log('Sorry,Antonyms for the word not available');
	}
	printSeparator();

}


function displaySynonyms(wordsArray) {
	console.log('Synonyms:');
	if (wordsArray.length) {
		var filterArray = wordsArray.filter((x) => x.relationshipType == 'synonym');
		if (filterArray.length) {
			for (var i in filterArray[0].words) {
				console.log(+i+1+'. '+filterArray[0].words[i])
			}
		} else {
			console.log('Sorry,Synonyms for the word not available');
		}
	} else {
		console.log('Sorry,Synonyms for the word not available');
	}
	printSeparator();
}


function displayWordOfTheDay(word) {
	console.log('Word of the day:', word);
	printSeparator();
}


function displayDefinitionForWord(wordDefinitions, word, cb) {
	if (wordDefinitions.length) {
		var index = getRandomIndexForArray(wordDefinitions);
		console.log('Definition of the word :');
		console.log(wordDefinitions[index].text);
		printSeparator();
		cb(word);
	} else {
		cb(null);
	}
}

function displaySynonymForWord(wordsArray, word, cb) {
	debug(wordsArray, 'wordss');
	if (wordsArray.length) {
		var filterArray = wordsArray.filter((x) => x.relationshipType == 'synonym');
		debug(filterArray);
		if (filterArray.length) {
			var words = filterArray[0].words;
			console.log('Synonym of the word:');
			console.log(words[getRandomIndexForArray(words)])
			printSeparator();
			cb(word);
		} else {
			cb(null);
		}
	} else {
		cb(null);
	}
}

function displayAntonymForWord(wordsArray, word, cb) {
	if (wordsArray.length) {
		var filterArray = wordsArray.filter((x) => x.relationshipType == 'antonym');
		debug(filterArray);
		if (filterArray.length) {
		var index = getRandomIndexForArray(wordsArray);
			var words = filterArray[0].words;
			console.log('Antonym of the word:');
			console.log(words[getRandomIndexForArray(words)])
			printSeparator();
			cb(word);
		} else {
			cb(null);
		}
	} else {
		cb(null);
	}
}

function getRandomIndexForArray(array) {
	return Math.floor(Math.random() * array.length);;
}

function printSeparator(){
	console.log("======================================");
}

module.exports = {
	displayWordOfTheDay,
	displaySynonyms,
	displayAntonyms,
	displayExamples,
	displayDefinitons,
	displayAntonymForWord,
	displaySynonymForWord,
	displayDefinitionForWord,
	getRandomIndexForArray
}